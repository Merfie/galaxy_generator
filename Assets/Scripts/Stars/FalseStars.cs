﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FalseStars : MonoBehaviour {

    private Transform _tx;
    private ParticleSystem.Particle[] _starSystem;
    private float[] _angles;
    private float _radius;

    public int starCount = 100000;
    public float starMinSize = .35f;
    public float starMaxSize = .35f;
    public Color insideColor = Color.red; 



	// Use this for initialization
	void Start () {
        _tx = GetComponent<Transform>();
	}

    public void AdjustStarCount(float _newStarCount){
        starCount = (int)_newStarCount;
        CreateStars();
    }
	
    private void CreateStars()
    {
        _starSystem = new ParticleSystem.Particle[starCount];
        _angles = new float[starCount];
        _radius = 340f / starCount;
        for(int i = 0; i < starCount; i++){
            if (i % 2 == 1)
            {
                float angle = Random.value * Mathf.PI * 2;
                _angles[i] = angle;
                Vector3 _position = new Vector3(1.5f * Mathf.Cos(_angles[i]) * _radius * i, Mathf.Sin(_angles[i]) * _radius * i, 600);
                _position = Quaternion.AngleAxis(.004f * i, Vector3.forward) * _position;
                _starSystem[i].position = _position;
                _starSystem[i].startColor = new Color(Random.value, Random.value, Random.value, 1.0f);
                _starSystem[i].startSize = Random.value * starMaxSize + starMinSize;
            }
            else
            {
                float angle = Random.value * Mathf.PI * 2 ;
                _angles[i] = angle;
                Vector3 _position = new Vector3(1.5f * Mathf.Cos(_angles[i]) * _radius * i, Mathf.Sin(_angles[i]) * _radius * i, 600);
                _position = Quaternion.AngleAxis(90 + (.004f * i), Vector3.forward) * _position;
                _starSystem[i].position = _position;
                _starSystem[i].startColor = new Color(Random.value, Random.value, Random.value, 1.0f);
                _starSystem[i].startSize = Random.value * starMaxSize + starMinSize;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (_starSystem == null)
        {
            CreateStars();
        }
        else
        {
            for (int i = 0; i < starCount; i++)
            {
                //_angles[i] -= .004f;
                //Vector3 _position = new Vector3(1.5f * Mathf.Cos(_angles[i]) * _radius * i, Mathf.Sin(_angles[i]) * _radius * i, 600);
                //_position = Quaternion.AngleAxis(.004f * i, Vector3.forward) * _position;
                //_starSystem[i].position = _position;
                //_starSystem[i].startSize = Random.value * starMaxSize + starMinSize;
            }
        }
        GetComponent<ParticleSystem>().SetParticles(_starSystem, starCount);
    }
}
