﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NaiveFillerStar : Star {

    private float xTilt;
    private float yTilt;
    private float radiusMagnitude;
    Quaternion _rotation;

    // Use this for initialization
    public NaiveFillerStar() {
        xTilt = (Random.value * 2) - 1;
        yTilt = (Random.value * 2) - 1;
        OrbitPosition = Random.value * 360;
        radiusMagnitude = Random.value;
        AngularVelocity = .02f;
        _rotation = Quaternion.AngleAxis(xTilt * 0, Vector3.up) * Quaternion.AngleAxis(yTilt * 0, Vector3.forward);
    }

    public void UpdateCoreOrbit(float _radius, float _eccentricity, float _xTiltSize, float _yTiltSize, float _timeDelta)
    {
        OrbitPosition = (OrbitPosition + AngularVelocity * _timeDelta) % 360;
        Position.x = (float)System.Math.Cos(OrbitPosition) * _radius * radiusMagnitude;
        Position.y = _eccentricity * (float)System.Math.Sin(OrbitPosition) * _radius * radiusMagnitude;
        Position.z = 600;
        Position = _rotation * Position;
    }
    public void UpdateRimOrbit(float _outsideRadius, float _insideRadius, float _eccentricity, float _xTiltSize, float _yTiltSize, float _timeDelta)
    {
        OrbitPosition = (OrbitPosition + AngularVelocity * _timeDelta) % 360;
        Position.x = (float)System.Math.Cos(OrbitPosition) * ((_outsideRadius - _insideRadius) * radiusMagnitude + _insideRadius);
        Position.y = _eccentricity * (float)System.Math.Sin(OrbitPosition) * ((_outsideRadius - _insideRadius) * radiusMagnitude + _insideRadius);
        Position.z = 600;
        Position = _rotation * Position;
    }
}
