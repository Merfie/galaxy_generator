﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Star {

    public Vector3 Position;
    public float AngularVelocity;
    public float OrbitPosition;
}
