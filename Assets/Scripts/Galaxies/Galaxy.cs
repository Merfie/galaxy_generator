﻿using System;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;

public class Galaxy : MonoBehaviour {

    public float CoreRadius;
    public int CoreDensity;
    public float CoreXTilt;
    public float CoreYTilt;
    public float BarSize;
    public ParticleSystem CoreParticleSystem;
    /* Color Picker will need to be implimented before we can offer this
    public Color CoreColor;
    */
    public float RimRadius;
    public float RimGap;
    public int RimDensity;
    public float Eccentricity = 1f;
    public float SpiralMagnitude;
    public float MajorArmLength;
    public float MinorArmLength;
    public float WobbleFrequency;
    public float WobbleAmplitude;
    public ParticleSystem RimParticleSystem;
    /* Color Picker will need to be implimented before we can offer this
    public Color RimColor;
    public Color BurstColor;
    */

    private NaiveFillerStar[] coreFillerStars;
    private NaiveFillerStar[] rimFillerStars;
    private ParticleSystem.Particle[] coreStarSystem;
    private ParticleSystem.Particle[] rimStarSystem;


    // Use this for initialization
    void Start () {
        coreFillerStars = new NaiveFillerStar[CoreDensity];
        rimFillerStars = new NaiveFillerStar[RimDensity];
        coreStarSystem = new ParticleSystem.Particle[CoreDensity];
        rimStarSystem = new ParticleSystem.Particle[RimDensity];
        CreateFillerStars();
    }

	// Update is called once per frame
	void Update () {
        float _timeDelta = Time.deltaTime;
        for (int i = 0; i < coreFillerStars.Length; i++)
        {
            coreFillerStars[i].UpdateCoreOrbit(CoreRadius, BarSize, CoreXTilt, CoreYTilt, _timeDelta);
            coreStarSystem[i].position = coreFillerStars[i].Position;
        }
        for (int i = 0; i < rimFillerStars.Length; i++)
        {
            rimFillerStars[i].UpdateRimOrbit(RimRadius, CoreRadius, BarSize, CoreXTilt, CoreYTilt, _timeDelta);
            rimStarSystem[i].position = rimFillerStars[i].Position;
        }
        CoreParticleSystem.SetParticles(coreStarSystem, coreStarSystem.Length);
        RimParticleSystem.SetParticles(rimStarSystem, rimStarSystem.Length);
        Debug.Log(RimDensity);
    }

    public void UpdateCoreDensity(float NewCoreDensity)
    {
        int _newCoreDensity = (int)NewCoreDensity;
        if (_newCoreDensity > CoreDensity)
        {
            NaiveFillerStar[] _newFillerStars = new NaiveFillerStar[_newCoreDensity - CoreDensity];
            ParticleSystem.Particle[] _newStarSystem = new ParticleSystem.Particle[_newCoreDensity - CoreDensity];
            for (int i = 0; i < _newFillerStars.Length; i++)
            {
                _newFillerStars[i] = new NaiveFillerStar();
                _newStarSystem[i] = new ParticleSystem.Particle();
                _newStarSystem[i].startColor = Color.red;
                _newStarSystem[i].startSize = 2;
            }
            coreFillerStars = coreFillerStars.Concat(_newFillerStars).ToArray();
            coreStarSystem = coreStarSystem.Concat(_newStarSystem).ToArray();
        }
        else if (_newCoreDensity < CoreDensity)
        {
            Array.Resize(ref coreFillerStars, _newCoreDensity);
            Array.Resize(ref coreStarSystem, _newCoreDensity);
        }
        CoreDensity = _newCoreDensity;
    }
    public void UpdateRimDensity(float NewRimDensity)
    {
        
        int _newRimDensity = (int)NewRimDensity;
        if (_newRimDensity > RimDensity)
        {
            NaiveFillerStar[] _newFillerStars = new NaiveFillerStar[_newRimDensity - RimDensity];
            ParticleSystem.Particle[] _newStarSystem = new ParticleSystem.Particle[_newRimDensity - RimDensity];
            for (int i = 0; i < _newFillerStars.Length; i++)
            {
                _newFillerStars[i] = new NaiveFillerStar();
                _newStarSystem[i] = new ParticleSystem.Particle();
                _newStarSystem[i].startColor = Color.red;
                _newStarSystem[i].startSize = 2;
            }
            rimFillerStars = rimFillerStars.Concat(_newFillerStars).ToArray();
            rimStarSystem = rimStarSystem.Concat(_newStarSystem).ToArray();
        }
        else if (_newRimDensity < RimDensity)
        {
            Array.Resize(ref rimFillerStars, _newRimDensity);
            Array.Resize(ref rimStarSystem, _newRimDensity);
        }
        RimDensity = _newRimDensity;
    }
    public void UpdateCoreRadius(float NewCoreRadius)
    {
        CoreRadius = NewCoreRadius;
    }
    public void UpdateBarSize(float NewBarSize)
    {
        BarSize = NewBarSize;
    }
    private void CreateFillerStars()
    {
        float _timeDelta = Time.deltaTime;
        for (int i = 0; i < CoreDensity; i++)
        {
            NaiveFillerStar _star = new NaiveFillerStar();
            coreFillerStars[i] = _star;
            _star.UpdateCoreOrbit(CoreRadius, BarSize, CoreXTilt, CoreYTilt, _timeDelta);
            coreStarSystem[i].position = _star.Position;
            coreStarSystem[i].startColor = Color.red;
            coreStarSystem[i].startSize = 2;
        }
        for (int i = 0; i < RimDensity; i++)
        {
            NaiveFillerStar _star = new NaiveFillerStar();
            rimFillerStars[i] = _star;
            _star.UpdateRimOrbit(RimRadius, CoreRadius, Eccentricity, CoreXTilt, CoreYTilt, _timeDelta);
            rimStarSystem[i].position = _star.Position;
            rimStarSystem[i].startColor = Color.green;
            rimStarSystem[i].startSize = 2;
        }
    }
}
